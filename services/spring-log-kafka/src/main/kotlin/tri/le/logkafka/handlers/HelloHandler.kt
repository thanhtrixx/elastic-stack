package tri.le.logkafka.handlers

import org.apache.logging.log4j.LogManager
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse
import org.springframework.web.reactive.function.server.ServerResponse.ok
import org.springframework.web.reactive.function.server.body
import reactor.core.publisher.Mono
import reactor.core.publisher.toMono

@Component
class HelloHandler {

    private val log = LogManager.getLogger()

    fun sayHello(req: ServerRequest): Mono<ServerResponse> {

        log.traceEntry()

        return log.traceExit(ok().body("Hello from Spring".toMono()))
    }
}
