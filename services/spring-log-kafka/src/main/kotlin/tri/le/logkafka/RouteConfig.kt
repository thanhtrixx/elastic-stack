package tri.le.logkafka

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.reactive.function.server.router
import tri.le.logkafka.handlers.HelloHandler


@Configuration
class RouteConfig(
    private val helloHandler: HelloHandler
) {

    @Bean
    fun router() = router {
        "/api".nest {
            GET("/hello", helloHandler::sayHello)
        }
    }
}
