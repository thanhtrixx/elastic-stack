package tri.le.logfile

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.reactive.function.server.router
import tri.le.logfile.handlers.HelloHandler


@Configuration
class RouteConfig {

    @Bean
    fun router(
        helloHandler: HelloHandler
    ) = router {
        "/api".nest {
            GET("/hello", helloHandler::sayHello)
        }
    }
}
