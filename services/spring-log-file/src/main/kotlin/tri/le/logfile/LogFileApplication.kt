package tri.le.logfile

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class LogFileApplication

fun main(args: Array<String>) {
    runApplication<LogFileApplication>(*args)
}
