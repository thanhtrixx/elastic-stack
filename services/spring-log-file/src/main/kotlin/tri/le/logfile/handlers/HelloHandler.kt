package tri.le.logfile.handlers

import org.apache.logging.log4j.LogManager
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse
import org.springframework.web.reactive.function.server.ServerResponse.ok
import org.springframework.web.reactive.function.server.body
import reactor.core.publisher.Mono
import reactor.core.publisher.toMono
import java.util.concurrent.atomic.AtomicLong

@Component
class HelloHandler {

    private val log = LogManager.getLogger()

    private val counter = AtomicLong()

    fun sayHello(req: ServerRequest): Mono<ServerResponse> {

        log.traceEntry()

        log.info("Log in Handler. Counter: ${counter.getAndIncrement()}")
        log.info(counter)

        return log.traceExit(ok().body("Hello from Spring".toMono()))
    }
}
